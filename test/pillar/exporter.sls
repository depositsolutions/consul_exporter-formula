consul_exporter:
  bin_dir: '/usr/bin'
  dist_dir: '/opt/consul_exporter/dist'
  version: '0.4.0'
  service: True
  service_user: 'consul_exporter'
  service_group: 'consul_exporter'
  consul_adress: 127.0.0.1:8500
  acl_token: 4A3D4A92-E8B6-4167-8681-F22D521AAB3E
#  options:
#    - 'version'
